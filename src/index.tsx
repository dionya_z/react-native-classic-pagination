// @ts-ignore
import Paginator from 'paginator';
import React, { useEffect } from 'react';
import { StyleSheet, View, ViewProps } from 'react-native';

type ButtonProps = {
  isActive?: boolean;
  pageNumber: number;
  pageText?: string;
  onPress: (pageNumber: number) => void;
  isDisabled?: boolean;
};

type PaginationProps = {
  totalItemsCount: number;
  onChange: (props: any) => void;
  activePage: number;
  itemsCountPerPage: number;
  pageRangeDisplayed: number;
  prevPageText?: string;
  nextPageText?: string;
  lastPageText?: string;
  firstPageText?: string;
  hideDisabled?: boolean;
  hideNavigation?: boolean;
  hideFirstLastPages?: boolean;
  containerStyle?: ViewProps;
  arrowsOnly?: boolean;
  isLoading?: boolean;
  renderItem: (props: ButtonProps) => any;
};

export const Pagination = ({
  totalItemsCount,
  onChange,
  activePage,
  itemsCountPerPage,
  pageRangeDisplayed,
  prevPageText,
  nextPageText,
  lastPageText,
  firstPageText,
  hideDisabled,
  hideNavigation,
  hideFirstLastPages,
  containerStyle,
  arrowsOnly,
  isLoading,
  renderItem,
}: PaginationProps) => {
  const [buttons, setButtons] = React.useState([]);

  const Page = (props: ButtonProps) =>
    renderItem({
      ...props,
      onPress: props.isDisabled ? () => {} : props.onPress,
    });

  const isFirstPageVisible = (has_previous_page: boolean) =>
    !(hideFirstLastPages || (hideDisabled && !has_previous_page));

  const isPrevPageVisible = (has_previous_page: boolean) =>
    !(hideNavigation || (hideDisabled && !has_previous_page));

  const isNextPageVisible = (has_next_page: boolean) =>
    !(hideNavigation || (hideDisabled && !has_next_page));

  const isLastPageVisible = (has_next_page: boolean) =>
    !(hideFirstLastPages || (hideDisabled && !has_next_page));

  const buildPages = () => {
    const paginationInfo = new Paginator(
      itemsCountPerPage,
      pageRangeDisplayed,
    ).build(totalItemsCount, activePage);

    const pages = [];

    if (!arrowsOnly) {
      for (
        let i = paginationInfo.first_page;
        i <= paginationInfo.last_page;
        i++
      ) {
        pages.push(
          <Page
            isActive={i === activePage}
            key={i}
            pageNumber={i}
            pageText={i + ''}
            onPress={() => onChange(i)}
            isDisabled={isLoading}
          />,
        );
      }
    }

    isPrevPageVisible(paginationInfo.has_previous_page) &&
      pages.unshift(
        <Page
          key={'prev' + paginationInfo.previous_page}
          pageNumber={paginationInfo.previous_page}
          onPress={() => onChange(activePage - 1)}
          pageText={prevPageText}
          isDisabled={!paginationInfo.has_previous_page || isLoading}
        />,
      );

    isFirstPageVisible(paginationInfo.has_previous_page) &&
      pages.unshift(
        <Page
          key="first"
          pageNumber={1}
          onPress={() => onChange(1)}
          pageText={firstPageText}
          isDisabled={!paginationInfo.has_previous_page || isLoading}
        />,
      );

    isNextPageVisible(paginationInfo.has_next_page) &&
      pages.push(
        <Page
          key={'next' + paginationInfo.next_page}
          pageNumber={paginationInfo.next_page}
          onPress={() => onChange(activePage + 1)}
          pageText={nextPageText}
          isDisabled={!paginationInfo.has_next_page || isLoading}
        />,
      );

    isLastPageVisible(paginationInfo.has_next_page) &&
      pages.push(
        <Page
          key="last"
          pageNumber={paginationInfo.total_pages}
          onPress={() => onChange(totalItemsCount)}
          pageText={lastPageText}
          isDisabled={
            paginationInfo.current_page === paginationInfo.total_pages ||
            isLoading
          }
        />,
      );

    setButtons(pages as any);
  };

  useEffect(() => {
    buildPages();
    // eslint-disable-next-line
  }, [activePage]);

  // @ts-ignore
  return <View style={[styles.container, containerStyle]}>{buttons}</View>;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
});
