# React Native Classic Pagination

## Installation

The pagination which allows switch between divide data in React Native.

```js
npm i react-native-classic-pagination
```

## Usage example

```js
import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import { Pagination } from 'react-native-classic-pagination';
import axios from 'axios';

const ITEMS_PER_PAGE = 10;
const VISIBLE_RANGE = 3;

const YourComponent = () => {
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  const getData = async pageNumber => {
    setIsLoading(true);

    try {
      setData(
        // I prefer to use axios but you can use fetch or something else
        (
          await axios.get(
            `${'YOUR_DATA_ENDPOINT'}?page_size=${ITEMS_PER_PAGE}&page=${currentPage}`,
          )
        ).data,
      );

      setCurrentPage(currentPage);
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    (async () => await getData(currentPage))();
  }, [currentPage]);

  return (
    <View>
      {data?.items && (
        <View>
          {data?.items?.map(item => (
            <View key={item.id}>
              <Text>{item.title}</Text>
            </View>
          ))}

          <Pagination
            activePage={currentPage}
            pageRangeDisplayed={VISIBLE_RANGE}
            totalItemsCount={data?.total}
            itemsCountPerPage={ITEMS_PER_PAGE}
            hideFirstLastPages
            prevPageText="Prev"
            nextPageText="Next"
            onChange={setCurrentPage}
            renderItem={props => (
              <BasePressable
                {...props}
                style={
                  {
                    // your custom component styles
                  }
                }
              >
                <BaseText fontWeight={props.isActive ? 'bold' : undefined}>
                  {props.pageText}
                </BaseText>
              </BasePressable>
            )}
          />
        </View>
      )}
    </View>
  );
};
```
